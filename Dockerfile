FROM nginx:1.17.1-alpine
COPY dist/dotenv-angular /usr/share/nginx/html
EXPOSE 4200