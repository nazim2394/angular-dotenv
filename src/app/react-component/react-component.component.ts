import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-react-component',
  templateUrl: './react-component.component.html',
  styleUrls: ['./react-component.component.scss']
})
export class ReactComponentComponent implements OnInit {

  public counter = 21;

  constructor() { }

  ngOnInit() {
  }

  public handleOnClick(stateCounter: number) {
    this.counter++;
  }

}
