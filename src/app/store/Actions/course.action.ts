import { Action } from '@ngrx/store';
import { CourseItem } from '../Models/courseItem.model';
import { CourseActionType } from '../Enums/store.enum';

export class AddItemAction implements Action {
  readonly type = CourseActionType.ADD_ITEM;
  // add an optional payload
  constructor(public payload: CourseItem) {}
}

export type CourseAction = AddItemAction;
