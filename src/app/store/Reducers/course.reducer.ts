// import the interface
import { CourseItem } from '../Models/courseItem.model';
import { CourseAction } from '../Actions/course.action';
import { CourseActionType } from '../Enums/store.enum';

// create a dummy initial state
const initialState: Array<CourseItem> = [
  {
    department: 'Computer Engineering',
    name: 'C++ Programming',
  },
];

export function courseReducer(
  state: Array<CourseItem> = initialState,
  action: CourseAction
) {
  switch (action.type) {
    case CourseActionType.ADD_ITEM:
      return [...state, action.payload];
    default:
      return state;
  }
}
