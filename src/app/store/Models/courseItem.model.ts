export interface CourseItem {
    department: string;
    name: string;
}
