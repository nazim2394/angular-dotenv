import { Component, ComponentFactoryResolver, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { DogsComponent } from '../dogs/dogs.component';
import { PlayersComponent } from '../players/players.component';

@Component({
  selector: 'app-masters',
  templateUrl: './masters.component.html',
  styleUrls: ['./masters.component.scss']
})
export class MastersComponent implements OnInit {

  masterName = '';

  @ViewChild('masterHost', { static: true, read: ViewContainerRef }) container: ViewContainerRef;

  constructor(private resolver: ComponentFactoryResolver) { }

  ngOnInit() {
  }

  masterSelected(event) {
    this.masterName = event.target.value;
    const compClass = this.getComponentToRender(this.masterName); // Get Component that is to be rendered
    if (compClass) {
      const factory = this.resolver.resolveComponentFactory(compClass); // Retrieve factory object for the given component

      this.container.clear(); // Remove old component
      this.container.createComponent(factory); // Render the component on the specific element ref
    }
  }

  getComponentToRender(masterName: string) {
    if (!masterName) {
      return null;
    }

    let component;

    switch (masterName) {
      case 'dog':
        component = DogsComponent;
        break;

      case 'player':
        component = PlayersComponent;
        break;

      default:
        component = null;
        break;
    }

    return component;
  }

}
