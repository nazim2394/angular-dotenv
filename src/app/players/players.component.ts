import { Component, OnInit } from '@angular/core';
import { PlayersService } from '../services/players.service';
import { Router } from '@angular/router';

interface PlayerListType {
  first_name: string;
  last_name: string;
  position: string;
}

@Component({
  selector: 'app-players',
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.scss']
})

export class PlayersComponent implements OnInit {
  playerList: PlayerListType[] = [];

  constructor(private playerService: PlayersService, private router: Router) { }

  ngOnInit() {
    this.playerService.players$.subscribe(res => {
      this.playerList = res;
      console.log(res);
    });
  }

  refreshCache() {
    this.playerService.updateData();
  }

  getPlayerDetails(id: number) {
    this.router.navigate(['player-details'], { state: { id } });
  }
}
