import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { PlayersComponent } from './players/players.component';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DogsComponent } from './dogs/dogs.component';
import { MastersComponent } from './masters/masters.component';
import { CustomReactComponentWrapperComponent } from './react-component/CustomReactComponentWrapper';
import { ReactComponentComponent } from './react-component/react-component.component';

// import ngx-translate and the http loader
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { StoreModule } from '@ngrx/store';
import { courseReducer } from './store/Reducers/course.reducer';
import { FormsModule } from '@angular/forms';
import { StateManagementComponent } from './state-management/state-management.component';
import { PlayerDetailsComponent } from './player-details/player-details.component';
import { HttpRequestInterceptor } from './core/http-request.interceptor';
import { EditorModule } from '@tinymce/tinymce-angular';
import { RxjsComponent } from './rxjs/rxjs.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PlayersComponent,
    DogsComponent,
    MastersComponent,
    CustomReactComponentWrapperComponent,
    ReactComponentComponent,
    StateManagementComponent,
    PlayerDetailsComponent,
    RxjsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    HttpClientModule,
    FormsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    StoreModule.forRoot({
      course: courseReducer,
    }),
    EditorModule
  ],
  entryComponents: [
    DogsComponent,
    PlayersComponent
  ],
  providers: [
    { 
      provide: HTTP_INTERCEPTORS, useClass: HttpRequestInterceptor, multi:true,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http);
}
