import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpHeaders,
    HttpResponse,
    HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable()
export class HttpRequestInterceptor implements HttpInterceptor {

    constructor() { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        const newHeaders = new HttpHeaders({
            "Content-Type": "application/json",
        });

        //clone request and change header
        request = request.clone({ headers: newHeaders });

        return next.handle(request)
            .pipe(
                map((res: HttpResponse<any>) => {
                    console.log("Passed through the interceptor in response");
                    return res
                 }),
                 catchError((error: HttpErrorResponse) => {
                    let errorMsg = '';
                    if (error.error instanceof ErrorEvent) {
                       console.log('This is client side error');
                       errorMsg = `Error: ${error.error.message}`;
                    } else {
                       console.log('This is server side error');
                       if(error.status == 404) {
                        errorMsg = `Data Not Found`;
                       } else {
                            errorMsg = `Error Code: ${error.status},  Message: ${error.message}`;
                       }
                    }
                    console.log(errorMsg);
                    return throwError(errorMsg);
                 })
            );
    }
}