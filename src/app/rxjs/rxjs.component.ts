import { Component, OnInit } from '@angular/core';
import { from, of, throwError, timer } from 'rxjs';
import { catchError, filter, map, mergeMap, switchMap, tap } from 'rxjs/operators';

@Component({
  selector: 'app-rxjs',
  templateUrl: './rxjs.component.html',
  styleUrls: ['./rxjs.component.scss']
})
export class RxjsComponent implements OnInit {

  rxjsOperators: string[] = [
    'map',
    'filter',
    'tap',
    'catchError',
    'switchMap',
    'mergeMap',
    'concatMap',
    'exhaustMap',
    'take',
    'first',
    'last',
    'skip',
    'debounceTime',
    'distinctUntilChanged',
    'startWith',
    'scan',
    'reduce',
    'pluck',
    'shareReplay',
    'observeOn',
    'subscribeOn',
    'timeInterval',
    'timeout',
    'toArray',
    'from',
    'of'
  ];

  selectedOperator: string = '';

  constructor() { }

  ngOnInit() {
  }

  onOperatorChange(event: any) {
    this.selectedOperator = event.target.value;
    this.operatorFunctions(this.selectedOperator);
  }

  operatorFunctions(operator: string) {
    switch (operator) {
      case 'map':
        this.mapOperator();
        break;

      case 'filter':
        this.filterOperator();
        break;

      case 'tap':
        this.tapOperator();
        break;

      case 'catchError':
        this.catchErrorOperator();
        break;

      case 'switchMap':
        this.switchMapOperator();
        break;

      case 'mergeMap':
        this.mergeMapOperator();
        break;

      case 'concatMap':
        this.concatMapOperator();
        break;

      case 'exhaustMap':
        this.exhaustMapOperator();
        break;

      case 'take':
        this.takeOperator();
        break;

      case 'first':
        this.firstOperator();
        break;

      case 'last':
        this.lastOperator();
        break;

      case 'skip':
        this.skipOperator();
        break;

      case 'debounceTime':
        this.debounceTimeOperator();
        break;

      case 'distinctUntilChanged':
        this.distinctUntilChangedOperator();
        break;

      case 'startWith':
        this.startWithOperator();
        break;

      case 'scan':
        this.scanOperator();
        break;

      case 'reduce':
        this.reduceOperator();
        break;

      case 'pluck':
        this.pluckOperator();
        break;

      case 'shareReplay':
        this.shareReplayOperator();
        break;

      case 'observeOn':
        this.observeOnOperator();
        break;

      case 'subscribeOn':
        this.subscribeOnOperator();
        break;

      case 'timeInterval':
        this.timeIntervalOperator();
        break;

      case 'timeout':
        this.timeoutOperator();
        break;

      case 'toArray':
        this.toArrayOperator();
        break;

      case 'from':
        this.fromOperator();
        break;

      case 'of':
        this.ofOperator();
        break;
    }
  }

  mapOperator() {   // Desc: Apply operation with each value from source

    /******* Example 1 ***************/
    const source = from([1, 2, 3, 4, 5]);  // emit (1,2,3,4,5)
    const example = source.pipe(map(val => val + 10));  // add 10 to each value
    example.subscribe(val => console.log(val)); // output: 11,12,13,14,15

    /******* Example 2 ***************/
    const source1 = from([
      { name: 'Joe', age: 30 },
      { name: 'Frank', age: 20 },
      { name: 'Ryan', age: 50 }
    ]);  // emit ({name: 'Joe', age: 30}, {name: 'Frank', age: 20},{name: 'Ryan', age: 50})
    const example1 = source1.pipe(map(res => res.name));  // grab each persons name, could also use pluck for this scenario
    example1.subscribe(val => console.log(val));  // output: "Joe","Frank","Ryan"

  }

  filterOperator() {   // Desc: Emit values that pass the provided condition

    /******* Example 1 ***************/
    const source = from([1, 2, 3, 4, 5]);   // emit (1,2,3,4,5)
    const example = source.pipe(filter(num => num % 2 === 0)); // filter even numbers
    example.subscribe(val => console.log(`Even number: ${val}`));  // output: "Even number: 2", "Even number: 4"

    /******* Example 2 ***************/
    const source1 = from([
      { name: 'Joe', age: 31 },
      { name: 'Bob', age: 25 }
    ]);  // emit ({name: 'Joe', age: 31}, {name: 'Bob', age:25})
    const example1 = source1.pipe(filter(person => person.age >= 30)); // filter out people with age under 30
    example1.subscribe(val => console.log(val));  // output: { name: 'Joe', age: 31 }

  }

  tapOperator() {   // Desc: Perform actions such as logging, 'tap' does not transform values

    const source = from([1, 2, 3, 4, 5]);
    const example = source.pipe(
      tap(val => console.log(`BEFORE MAP: ${val}`)),
      map(val => val + 10),
      tap(val => console.log(`AFTER MAP: ${val}`))
    ); // log values from source with 'tap'
    example.subscribe(val => console.log(val));

  }

  catchErrorOperator() {   // Desc: Handle errors in an observable sequence

    /******* Example 1 ***************/
    const source = throwError('This is an error!');  // returning observable with error message
    const example = source.pipe(catchError(val => of(`I caught: ${val}`)));
    example.subscribe(val => console.log(val));  // output: 'I caught: This is an error'

    /******* Example 2 ***************/
    const myBadPromise = () => new Promise((resolve, reject) => reject('Rejected!'));  //create promise that immediately rejects
    const source1 = from(myBadPromise());  // Execute Promise
    const example1 = source1.pipe(catchError(error => of(`Bad Promise: ${error}`))  // catch rejected promise, returning observable containing error message
    );
    example1.subscribe(val => console.log(val));  // output: 'Bad Promise: Rejected'

  }

  switchMapOperator() {   // Desc: Map to observable, complete previous inner observable, emit values

    /******* Example 1 ***************/
    let sourceObservable = from([1, 2, 3, 4]);
    let innerObservable = from(['A', 'B', 'C', 'D']);

    // The project function is the first argument to the switchMap. It takes the values from the sourceObservable. 
    // For each value, it receives from the sourceObservable (i. e. 1,2,3,4) it creates a new observable i.e. innerObservable.
    sourceObservable.pipe(
      switchMap(val => { // Map to source observable
        console.log('Source value ' + val)
        console.log('starting new observable')
        return innerObservable // emit inner observable value for each source value
      })
    ).subscribe(ret => {
      console.log('Recd ' + ret);
    })
  }

  mergeMapOperator() {   // Desc: Map to observable, emit values from source as well as inner observable

    const myPromise = val => new Promise(resolve => resolve(`${val} World From Promise!`));
    const source$ = of('Hello');  // emit 'Hello'
    source$
      .pipe(mergeMap(val => myPromise(val)))
      .subscribe(val => console.log(val)); // output: 'Hello World From Promise'
      
  }

  concatMapOperator() {   // Desc: Emit values that pass the provided condition
  }

  exhaustMapOperator() {   // Desc: Emit values that pass the provided condition
  }

  takeOperator() {   // Desc: Emit values that pass the provided condition
  }

  firstOperator() {   // Desc: Emit values that pass the provided condition
  }

  lastOperator() {   // Desc: Emit values that pass the provided condition
  }

  skipOperator() {   // Desc: Emit values that pass the provided condition
  }

  debounceTimeOperator() {   // Desc: Emit values that pass the provided condition
  }

  distinctUntilChangedOperator() {   // Desc: Emit values that pass the provided condition
  }

  startWithOperator() {   // Desc: Emit values that pass the provided condition
  }

  scanOperator() {   // Desc: Emit values that pass the provided condition
  }

  reduceOperator() {   // Desc: Emit values that pass the provided condition
  }

  pluckOperator() {   // Desc: Emit values that pass the provided condition
  }

  shareReplayOperator() {   // Desc: Emit values that pass the provided condition
  }

  observeOnOperator() {   // Desc: Emit values that pass the provided condition
  }

  subscribeOnOperator() {   // Desc: Emit values that pass the provided condition
  }

  timeIntervalOperator() {   // Desc: Emit values that pass the provided condition
  }

  timeoutOperator() {   // Desc: Emit values that pass the provided condition
  }

  toArrayOperator() {   // Desc: Emit values that pass the provided condition
  }

  fromOperator() {   // Desc: Emit values that pass the provided condition
  }

  ofOperator() {   // Desc: Emit values that pass the provided condition
  }


}
