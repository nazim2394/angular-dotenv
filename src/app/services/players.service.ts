import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, timer } from 'rxjs';
import { mergeMap, shareReplay, map } from 'rxjs/operators';

const REFRESH_TIMER_INTERVAL = 30000;

@Injectable({
  providedIn: 'root'
})
export class PlayersService {

  private playersData$ = new BehaviorSubject<void>(undefined);

  apiURL = 'https://www.balldontlie.io/api/v1/players';

  apiRequest$ = this.http.get<any[]>(this.apiURL).pipe(
    map((value: any) => {
      return value.data.map((player) => ({
        ...player,
        fullName: `${player.first_name} ${player.last_name
          } ${Date.now().toFixed()}`,
      }));
    })
  );

  public players$ = this.playersData$.pipe(
    mergeMap(() => this.apiRequest$),
    shareReplay(1)
  );

  timer$ = timer(0, REFRESH_TIMER_INTERVAL).subscribe(res => {
    this.updateData();
  });

  constructor(private http: HttpClient) { }

  updateData() {
    this.playersData$.next();
  }

  getPlayerDetails(id: number): Observable<any> {
    return this.http.get<any>(`https://www.balldontlie.io/api/v1/players/${id}`);
  }
}
