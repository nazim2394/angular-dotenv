import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { CourseItem } from '../store/Models/courseItem.model';
import { Store } from '@ngrx/store';
import { AppState } from '../store/Models/state.model';
import { NgForm } from '@angular/forms';
import { AddItemAction } from '../store/Actions/course.action';

@Component({
  selector: 'app-state-management',
  templateUrl: './state-management.component.html',
  styleUrls: ['./state-management.component.scss']
})
export class StateManagementComponent implements OnInit {

  courseItems: CourseItem[];

  constructor(private store: Store<AppState>) { }

  ngOnInit(): void {
    this.getCourseItems();
  }

  addCourse(form: NgForm) {
    this.store.dispatch(new AddItemAction(form.value));
    form.reset();
    this.getCourseItems();
  }

  getCourseItems() {
    this.store.subscribe((appState: AppState) => {
      this.courseItems = appState.course;
    });
  }

}
