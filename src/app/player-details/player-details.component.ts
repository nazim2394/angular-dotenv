import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PlayersService } from '../services/players.service';

@Component({
  selector: 'app-player-details',
  templateUrl: './player-details.component.html',
  styleUrls: ['./player-details.component.scss']
})
export class PlayerDetailsComponent implements OnInit {

  playerDetails: any = {};

  constructor(private activatedRoute: ActivatedRoute, private playerService: PlayersService, private route: Router) {
    // this.activatedRoute.params.subscribe(res => {
    //   console.log(res);
    //   if(res.id) {
    //     this.playerService.getPlayerDetails(res.id).subscribe(res => {
    //       this.playerDetails = res;
    //     })
    //   }
    // })
    // this.activatedRoute.queryParams.subscribe(res => {
    //   console.log(res);
    //   if(res.id) {
    //     this.playerService.getPlayerDetails(res.id).subscribe(res => {
    //       this.playerDetails = res;
    //     })
    //   }
    // })
    const state = this.route.getCurrentNavigation().extras.state;
    if(state.id) {
      this.playerService.getPlayerDetails(state.id).subscribe(res => {
              this.playerDetails = res;
    })
    }
    // this.activatedRoute.data.subscribe(res => {
    //   console.log(res);
    //   if(res.id) {
    //     this.playerService.getPlayerDetails(res.id).subscribe(res => {
    //       this.playerDetails = res;
    //     })
    //   }
    // })
  }

  ngOnInit() {
  }

}
