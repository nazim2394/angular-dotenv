import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { MastersComponent } from './masters/masters.component';
import { ReactComponentComponent } from './react-component/react-component.component';
import { StateManagementComponent } from './state-management/state-management.component';
import { PlayerDetailsComponent } from './player-details/player-details.component';
import { RxjsComponent } from './rxjs/rxjs.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'masters',
    component: MastersComponent
  },
  {
    path: 'react-component',
    component: ReactComponentComponent
  },
  {
    path: 'state-management',
    component: StateManagementComponent
  },
  {
    path: 'player-details',
    component: PlayerDetailsComponent
  },
  {
    path: 'rxjs',
    component: RxjsComponent
  },
  {
    path: '**',
    redirectTo: 'home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
