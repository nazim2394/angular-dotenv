export const environment = {
  baseUrl: process.env.NG_APP_BASE_URL,
  production: true
};
